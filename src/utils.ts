import {LOCALES, PRONUNCIATION} from './configs';

interface MyIPA {
  ipa?: string;
  word?: string;
  pron?: string;
}

export class Utils {

  constructor() {
  }

  static getInverseLocale = (locale: string): string =>
    locale == LOCALES.US.code ?
      LOCALES.GB.code : LOCALES.US.code;

  static getWordTypeByCode(code: string): string {
    let wordType = '';
    switch (parseInt(code)) {
      case 1:
        wordType = 'adv';
        break;
      case 2:
        wordType = 'pron';
        break;
      case 3:
        wordType = 'adj';
        break;
      case 4:
        wordType = 'verb';
        break;
      case 5:
        wordType = 'adj_adv';
        break;
      case 6:
        wordType = 'varnish';
        break;
      case 7:
        wordType = 'noun';
        break;
      case 8:
        wordType = 'exclamation';
        break;
      case 9:
        wordType = 'NOTE';
        break;
      case 10:
        wordType = 'PDF';
        break;
      default:
        break;
    }
    return wordType;
  }

  static getPronunciation(code: string): MyIPA {

    const ipa: MyIPA = {};

    if (PRONUNCIATION.consonants[code]
      || PRONUNCIATION.vowels.US[code]
      || PRONUNCIATION.vowels.GB[code]) {

      ipa['ipa'] = code;
      if (PRONUNCIATION.consonants[code]) {
        ipa['word'] = PRONUNCIATION.consonants[code].word;
        ipa['pron'] = PRONUNCIATION.consonants[code].pron;
      }
      else if (PRONUNCIATION.vowels.US[code]) {
        ipa['word'] = PRONUNCIATION.vowels.US[code].word;
      }
      else if (PRONUNCIATION.vowels.GB[code]) {
        ipa['word'] = PRONUNCIATION.vowels.GB[code].word;
      }
    }
    return ipa;
  }

  static htmlDecode = (input: string): string =>
    input ? String(input).replace(/<[^>]+>/gm, '') : '';


  static bolder(sentence: string, matchingText: string | string[]): string {
    if (!matchingText || matchingText.length==0)
      return sentence;
    const tagOpen = '<b>';
    const tagClose = '</b>';
    const matchRegex = new RegExp(/<b>/, 'ig');
    let result = '';
    try {
      if (Array.isArray(matchingText)) {
        // to prevent lost of cases, we should search with the longer match text first
        // i.e, search: i in ine
        // text: In an Ice inevitable cube
        matchingText.sort((a: string, b: string) => a.length - b.length).reverse();
        matchingText.map(match => {
          const re = new RegExp(match, 'ig');
          if (match.length == 0)
            return;
          if (match.indexOf('<') > -1
            || match.indexOf('>') > -1
            || match.indexOf('/') > -1)
            return;
          if (result.length == 0)
            result = sentence.replace(new RegExp(match, 'ig'), tagOpen + match + tagClose);
          else {
            // try to skip replace the already highlighted text
            const indexArr = [];
            const indexes= [];
            let match: RegExpExecArray;
            while (match = matchRegex.exec(result)) {
              indexArr.push([match.index, match.index + match[0].length]);
            }
            while (match = re.exec(result)) {
              const iBegin = match.index;
              const iEnd = iBegin + match[0].length;
              let reserved = false;
              for (let i = 0; i < indexArr.length; i++) {
                const rBegin = indexArr[i][0];
                const rEnd = indexArr[i][1];
                if ((rBegin < iBegin && iBegin < rEnd) || (rBegin < iEnd && iEnd < rEnd)) {
                  reserved = true;
                  break;
                }
              }
              if (!reserved) {  // this matching is not reserved, we can highlight
                indexes.push([match.index, match[0].length]);
              }
            }
            let plusLength = 0;
            for (let k = 0; k < indexes.length; k++){
              const index = indexes[k][0] + plusLength;
              const matchStr = result.substr(index, indexes[k][1]);
              const replacedStr = tagOpen + matchStr + tagClose;
              result = Utils.replaceAtIndex(result, index, matchStr, replacedStr);
              plusLength += 7;  // length of <b></b>
            }
            // result = result.replace(new RegExp(match, 'ig'), tagOpen + match + tagClose);
          }
        });
      }
      else {
        result = sentence.replace(new RegExp(matchingText, 'ig'), tagOpen + matchingText + tagClose);
      }
    }
    catch (ex) {
      console.error('ERROR Highlight text', ex);
    }
    return result;
  }
  static replaceAtIndex(originalStr: string, index: number, match: string, replace: string): string {
    const firstPart = originalStr.slice(0, index);
    const newPart = originalStr.slice(index).replace(match, replace);
    return firstPart + newPart;
  }
}
