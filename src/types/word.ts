import {Utils} from '../utils';

interface IWordExplanation {
  exp?: string;
  sentence?: string;
}
interface IWordUsage {
  usageOrder: number;
  exps?: IWordExplanation[];
}
interface IWordUsageList {
  [usage: string] : IWordUsage;
}

interface IWordMean {
  meanOrder: number;
  meaning?: string;
  usages?: IWordUsageList;
  usageKeyArr?: string[];
}

interface IWordRefs {
  pageType: string,
  meanOrder: string,
  usageOrder: string,
  page: string
}

interface WordInterface {
  wordId: number;
  word: string;
  ipa: {
    US?: string,
    GB?: string,
  };
  type?: string;
  typeLabel?: string;
  timestamp?: number;
  isBookmarked?: boolean;
  // bewlow part is for collocation
  means?: IWordMean[];
  refs: IWordRefs[];

  setIPA(locale: string, ipa: string): void;

}

class Word implements WordInterface {

  wordId: number;
  word: string;
  ipa: { US?: string; GB?: string; };
  typeLabel?: string;
  timestamp?: number;
  isBookmarked: boolean;

  private _type?: string;

  means: IWordMean[];
  refs: IWordRefs[];

  constructor(wordId: number, word: string) {
    this.wordId = wordId;
    this.word = word;
    this.ipa = {};
    this.refs = [];
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
    this.typeLabel = value ? Utils.getWordTypeByCode(value) : '';
  }

  setIPA(locale: string, ipa: string) {
    this.ipa[locale] = ipa;
  }
}

interface WordList {
  [wordId: number] : Word;
}

export {Word, WordList, IWordMean, IWordUsage, IWordExplanation, IWordRefs}
