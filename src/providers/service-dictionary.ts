import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {SQLite, SQLiteDatabaseConfig, SQLiteObject} from '@ionic-native/sqlite';
import {ServiceStorage} from './atom/service-storage';
import {CONFIGS, DICTS, LOCALES} from '../configs';
import {Word, WordList, IWordMean, IWordRefs} from '../types/word';
import {Utils} from '../utils';

const DB_US: SQLiteDatabaseConfig = {
  name: DICTS.cmudict_min.dbFileName,
  location: CONFIGS.db_location_default
};
const DB_GB: SQLiteDatabaseConfig = {
  name: DICTS.cuv2dict_min.dbFileName,
  location: CONFIGS.db_location_default
};
const DB_Collocation: SQLiteDatabaseConfig = {
  name: DICTS.oxford_collocation.dbFileName,
  location: CONFIGS.db_location_default
};

@Injectable()
export class ServiceDictionary {

  dict: SQLiteObject;
  dictUS: SQLiteObject;
  dictGB: SQLiteObject;

  constructor(private sqlite: SQLite,
              public serviceStorage: ServiceStorage) {
  }

  openDatabase(): Promise<boolean> {
    return new Promise(resolve => {
      this.serviceStorage.isDbDeployed().then(isDeployed => {
        if (isDeployed) {
          Promise.all([
            this.sqlite.create(DB_Collocation),
            this.sqlite.create(DB_US),
            this.sqlite.create(DB_GB)
          ]).then((values) => {
            this.dict = values[0];
            this.dictUS = values[1];
            this.dictGB = values[2];
            resolve(true);
          }, (errs) => {
            console.error('ServiceDictionary openDatabase ERROR = ', errs);
            resolve(false);
          });
        }
        else
          resolve(false);
      });
    });
  }

  searchWords(searchText: string, index: number, limit: number, isRandom = true): Promise<WordList> {
    const wordList: WordList = {};
    let max = DICTS.oxford_collocation.count,
      min = 1;
    let query = 'SELECT wordId, word, type FROM word';
    let parameters: [string | number] = [index, limit];

    return new Promise(resolve => {
      if (!this.dict)
        resolve(wordList);
      else {
        if (searchText && searchText.length > 0) {
          const queryText = searchText.replace(/\s/ig, '%') + '%';
          query += ' WHERE word LIKE ? LIMIT ?,?';
          parameters = [queryText, index, limit];
        }
        else if (isRandom == true) {
          const randomIds: number[] = [];
          while (randomIds.length < limit) {
            const randomId = Math.floor(Math.random() * (max - min)) + min;
            if (randomIds.indexOf(randomId) < 0)
              randomIds.push(randomId);
          }
          query += ' WHERE wordId IN (' + randomIds + ') LIMIT ?,?';
        }
        else {
          query += ' LIMIT ?,?';
        }
        this.dict.executeSql(query, parameters).then(result => {
          for (let i = 0; i < result.rows.length; i++) {
            const aWord = new Word(result.rows.item(i).wordId, result.rows.item(i).word);
            aWord.type = result.rows.item(i).type;
            wordList[aWord.wordId] = aWord;
          }
          resolve(wordList);
        }, (err) => {
          console.error('Unable to execute sql searchWords = ' + JSON.stringify(err));
          resolve(wordList);
        });
      }
    });
  }

  loadPronunciations(wordList: WordList, locale: string): Promise<boolean> {

    const currentDict = locale == LOCALES.US.code ? this.dictUS : this.dictGB;

    let parameters = [];
    for (const key in wordList) {
      const aWord: Word = wordList[key];
      if (!aWord.ipa)
        aWord.ipa = {};
      // only load from the database if the locale ipa is not set
      if (!aWord.ipa[locale])
        parameters.push(aWord.word);
    }
    return new Promise(resolve => {
      if (parameters.length > 0) {
        let queryInlineParams = '';
        const queryInlineArr = [];
        for (const param of parameters) {
          queryInlineParams += ',?';
          queryInlineArr.push(param.toLowerCase());
          if (param.includes(' ')) {
            queryInlineParams += ',?';
            queryInlineArr.push(param.toLowerCase().replace(/\s/g, '-'));
          }
        }
        queryInlineParams = queryInlineParams.substr(1);
        let query = 'SELECT word, ipa' + (locale == LOCALES.GB.code ? ', type' : '') ;
        query = query + ' FROM word WHERE LOWER(word) IN (' + queryInlineParams + ');';
        currentDict.executeSql(query, queryInlineArr).then(result => {
          for (let i = 0; i < result.rows.length; i++) {
            const word = result.rows.item(i).word.toLowerCase();
            const ipa = result.rows.item(i).ipa;
            const type = locale == LOCALES.GB.code ? result.rows.item(i).type : '';
            for (const key of Object.keys(wordList)) {
              const aWord: Word = wordList[key];
              const lWord = aWord.word.toLowerCase();
              // in case the locale is GB, it provides the pronunciation of of word's type,
              // we need to check if the types are matched first
              if (locale == LOCALES.GB.code && type && !type.includes(aWord.type))
                continue;
              // if the type is matched, check if the words are matched
              if (lWord == word || lWord.replace(/\s/g, '-') == word) {
                if (!aWord.ipa[locale] || aWord.ipa[locale].length == 0)
                  aWord.ipa[locale] = ipa;
              }
            }
          }
          resolve(true);
        }, (error) => {
          console.error('loadPronunciations query error = ' + JSON.stringify(error));
          resolve(false);
        });
      }
      else {
        resolve(true);
      }
    });
  }

  /**
   * Return a list of meanings of provided Word (id)
   * @param wordId
   * @returns {Promise<IWordMean>}
   */
  loadWordMeaning(wordId: string | number): Promise<IWordMean[]> {
    const query = 'SELECT meanOrder, meaning FROM meaning WHERE wordId = ?';
    const parameters = [wordId];

    const means: IWordMean[] = [];
    return new Promise<IWordMean[]>(resolve => {
      this.dict.executeSql(query, parameters).then(result => {
        for (let i = 0; i < result.rows.length; i++) {
          const meanOrder = result.rows.item(i).meanOrder;
          const wordMean: IWordMean = {
            meanOrder,
            meaning: Utils.htmlDecode(result.rows.item(i).meaning),
            usages: {},
            usageKeyArr: []
          };
          means.push(wordMean);
        }
        if (result.rows.length == 0)
          means.push({ meanOrder: 0, meaning: '', usages: {}, usageKeyArr: [] });
        resolve(means);
      }, (error) => {
        console.error('loadPronunciations query error = ' + JSON.stringify(error));
        resolve(means);
      });
    });
  }

  /**
   * Load the meanings & usages/explanations/sentences into a meaning-loaded Word
   * @param wordId
   * @param wordMeans
   * @returns {Promise<boolean>}
   */
  loadMeanUsage(wordId: number | string, wordMeans: IWordMean[]): Promise<boolean> {
    const meanArr = wordMeans.map(item => item.meanOrder);
    const query = 'SELECT meanOrder,usageOrder,usage,explanation,sentence FROM usage ' +
      'WHERE wordId = ? AND meanOrder IN (' + meanArr + ')';
    const parameters = [wordId];

    return new Promise<boolean>(resolve => {
      if (!meanArr || meanArr.length == 0)
        return resolve(true);
      this.dict.executeSql(query, parameters).then(result => {
        for (let i = 0; i < result.rows.length; i++) {
          const meanOrder = result.rows.item(i).meanOrder;
          const usageOrder = result.rows.item(i).usageOrder;
          const usage = result.rows.item(i).usage;
          const exp = result.rows.item(i).explanation;
          const sentence = result.rows.item(i).sentence;

          const wordMean = wordMeans.find( item => item.meanOrder==meanOrder );

          if (!wordMean.usages[usage]) wordMean.usages[usage] = {
            usageOrder,
            exps: []
          };
          if (wordMean.usageKeyArr.indexOf(usage) < 0)
            wordMean.usageKeyArr.push(usage);
          wordMean.usages[usage].exps.push({exp, sentence});
        }
        resolve(true);
      }, (error) => {
        console.error('loadPronunciations query error = ' + JSON.stringify(error));
        resolve(false);
      });
    });
  }

  loadWordRef(wordId: number | string): Promise<IWordRefs[]> {
    const query = 'SELECT meanOrder,usageOrder,pageType,page FROM ref_page WHERE wordId = ?';
    const parameters = [wordId];

    const wordRefs = [];
    return new Promise<IWordRefs[]>(resolve => {
      this.dict.executeSql(query, parameters).then(result => {

        for (let i = 0; i < result.rows.length; i++) {
          const meanOrder = result.rows.item(i).meanOrder;
          const usageOrder = result.rows.item(i).usageOrder;
          const pageType = result.rows.item(i).pageType;
          const page = result.rows.item(i).page;

          wordRefs.push({pageType, meanOrder, usageOrder, page});
        }

        resolve(wordRefs);
      }, function (error) {
        console.log('loadMeanUsage query error = ' + JSON.stringify(error));
        resolve(wordRefs);
      });
    });
  }

  findWord(word: string, type: string): Promise<Word> {
    let query = 'SELECT wordId FROM word WHERE word LIKE ? AND type = ?';
    let parameters = [word, type];

    return new Promise(resolve => {
      if (!this.dict)
        resolve(null);
      else {
        this.dict.executeSql(query, parameters).then(result => {
          if (result.rows.length == 1) {
            const aWord = new Word(result.rows.item(0).wordId, word);
            aWord.type = type;
            resolve(aWord);
          }
          else
            resolve(null);
        }, (err) => {
          console.error('Unable to execute sql searchWords = ' + JSON.stringify(err));
          resolve(null);
        });
      }
    });
  }

  searchSentences(searchText: string, index: number, limit: number): Promise<string[]>{
    const searchResults = [];
    return new Promise(resolve => {
      if (!this.dict)
        resolve(searchResults);
      else {
        let query = 'SELECT sentence FROM usage LIMIT ?,?';
        let parameters: any[] = [index, limit];
        if (searchText && searchText.length > 0) {
          query = 'SELECT sentence FROM usage WHERE sentence LIKE ? LIMIT ?,?';
          const queryText = '%' + searchText.replace(/\s/ig, '%') + '%';
          parameters.unshift(queryText);
        }
        this.dict.executeSql(query, parameters).then(result => {
          const searchTextArray = searchText ? searchText.split(' ') : [];
          for (let i = 0; i < result.rows.length; i++) {
            let sentence = result.rows.item(i).sentence;
            for (let searchStr of searchTextArray)
              sentence = Utils.bolder(sentence, searchStr);
            searchResults.push(sentence);
          }
          resolve(searchResults);
        }, (err) => {
          console.error('Unable to execute sql searchSentences = ' + JSON.stringify(err));
          resolve(searchResults);
        });
      }
    });
  }
}

