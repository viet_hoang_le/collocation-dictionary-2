import {Injectable} from '@angular/core';
import {Platform, PopoverController} from 'ionic-angular';
import {ServiceStorage} from './atom/service-storage';
import {Toast} from '@ionic-native/toast';
import {Network} from '@ionic-native/network';
import {TextToSpeech, TTSOptions} from '@ionic-native/text-to-speech';
import {AdMob, AdMobOptions} from '@ionic-native/admob';
import {AppRate} from '@ionic-native/app-rate';
import {StreamingAudioOptions, StreamingMedia} from '@ionic-native/streaming-media';
import {GoogleAnalytics} from '@ionic-native/google-analytics';
import {CONFIGS, TABLE, PLATFORM, STORE, LOCALES} from '../configs';
import {Word} from '../types/word';
import {Utils} from '../utils';

@Injectable()
export class ServiceHelper {

  countAdmob = 0;
  countLocaleTip = 0;
  isGoogleAnalyticsReady = false;

  constructor(private network: Network,
              private admob: AdMob,
              private appRate: AppRate,
              private streamingMedia: StreamingMedia,
              private tts: TextToSpeech,
              private toast: Toast,
              private ga: GoogleAnalytics,
              private platform: Platform,
              private popoverCtrl: PopoverController,
              private serviceStorage: ServiceStorage) {
  }

  /**
   * Check if the databases are not deployed
   * If not, download the zip file from remote and deploy it
   * @returns {Promise<T>}
   */
  deployDBs(): Promise<boolean> {
    return new Promise(resolve => {
      this.serviceStorage.isDbDeployed().then(isDeployed => {
        if (!isDeployed && this.checkIsConnected(true)) {
          this.serviceStorage.downloadDb().then(zipFilePath => {
            if (zipFilePath)
              this.serviceStorage.unzipDB(zipFilePath).then(isUnzipped => resolve(isUnzipped));
            else
              resolve(false);
          });
        }
        else
          resolve(true);
      });
    });
  }

  onclickWord(wordItem: Word,
              isNaturalTTS = false,
              locale = this.serviceStorage.setting.locale) {

    this.serviceStorage.setBookmarkHistory(TABLE.HISTORY, wordItem);

    if (!isNaturalTTS)
      this.tts_text(wordItem.word, locale);
    else
      this.naturalTTS(wordItem.word, locale);
  }

  onclickPopoverLocale(ev) {
    if (this.serviceStorage.isPopoverShow) {
      const popover = this.popoverCtrl.create('popover-locale');
      popover.present({ev});
    }
    else {
      const los = this.serviceStorage.setting.locales;
      let lo = this.serviceStorage.setting.locale;
      let localeFound = false;

      for (let i = 0; i < los.length; i++) {
        if (lo == los[i]) {
          lo = i == los.length - 1 ? los[0] : los[i + 1];
          localeFound = true;
          break;
        }
      }

      if (!localeFound)
        lo = los.length > 0 ? los[0] : LOCALES.US.code;

      this.serviceStorage.setStorage(STORE.locale, lo);
    }
  }

  checkIsConnected(isNotify = false): boolean {
    try {
      if (this.network.type == 'none') {
        if (isNotify)
          this.toast.showShortBottom('You need Internet connection to continue...').subscribe();
        return false;
      }
      return true;
    }
    catch (e) {
      console.error('checkIsConnected ERROR = ' + JSON.stringify(e));
      return false;
    }
  }

  naturalTTS(text: string, locale = this.serviceStorage.setting.locale) {
    if (this.checkIsConnected(true) && text && text.trim().length > 0) {
      const plainText = Utils.htmlDecode(text).toLowerCase();
      let localeVoice = '&voice=';
      if (locale.toUpperCase() === LOCALES.GB.code)
        localeVoice += CONFIGS.ttsVoiceGB[Math.floor(Math.random()*CONFIGS.ttsVoiceGB.length)];
      else
        localeVoice += CONFIGS.ttsVoiceUS[Math.floor(Math.random()*CONFIGS.ttsVoiceUS.length)];
      let speed = (this.serviceStorage.speed - 1) * 100;
      if (speed > 100) speed = 100;
      if (speed < -100) speed = -100;
      const rate = '&rate=' + speed + '%';
      const options: StreamingAudioOptions = {
        bgColor: '#FFFFFF',
        bgImage: '<SWEET_BACKGROUND_IMAGE>',
        bgImageScale: 'fit', // other valid values: "stretch"
        initFullscreen: false, // true(default)/false iOS only
        successCallback: function () {
          // console.log("Player closed without error.");
        },
        errorCallback: function (errMsg) {
          console.error('Error! ' + errMsg);
        }
      };
      const url = CONFIGS.ttsUrl + localeVoice + rate + '&text=' + plainText;
      this.streamingMedia.playAudio(url, options);

      this.showAdmobInterstitial();
    }
  }

  tts_text(text: string, locale = this.serviceStorage.setting.locale) {
    if (text && text.trim().length > 0) {
      const ttsOptions: TTSOptions = {
        text: Utils.htmlDecode(text).toLowerCase(),
        locale: 'en-' + locale.toUpperCase(),
        rate: this.serviceStorage.speed
      };
      this.tts.speak(ttsOptions)
        .catch((reason: any) => console.error(JSON.stringify(reason)));

      this.showAdmobInterstitial();

      this.showLocaleTip();

    }
  }

  private getAdmobId() {
    const admobid = {
      banner: '',
      interstitial: ''
    };
    if (this.platform.is(PLATFORM.android)) { // for android & amazon-fireos
      admobid.banner = CONFIGS.admobIds.android.banner;
      admobid.interstitial = CONFIGS.admobIds.android.interstitial;
    }
    else { // for ios
      admobid.banner = CONFIGS.admobIds.ios.banner;
      admobid.interstitial = CONFIGS.admobIds.ios.interstitial;
    }
    return admobid;
  }

  initInternetServices() {
    if (CONFIGS.is_display_ads && this.checkIsConnected()) {
      try {
        const adConfig: AdMobOptions = {
          adId: this.getAdmobId().banner,
          autoShow: false
        };
        this.admob.createBanner(adConfig);
        this.admob.onAdFailLoad().subscribe((e) => {
          console.error('Admob FAILED to createBanner = ' + JSON.stringify(e))
        });
        this.admob.onAdLoaded().subscribe(() => {
          this.admob.showBanner(this.admob.AD_POSITION.BOTTOM_CENTER);
        });
      } catch (err) {
        console.error('ERROR initInternetServices = ' + JSON.stringify(err));
      }
    }
  }

  showAdmobInterstitial(): void {
    if (CONFIGS.is_display_ads
      && this.countAdmob < 3
      && this.checkIsConnected()
    ) {
      try {
        this.countAdmob++;
        if (this.countAdmob == 3) {
          const adConfig: AdMobOptions = {
            adId: this.getAdmobId().interstitial,
            autoShow: true
          };
          this.admob.prepareInterstitial(adConfig);
        }
      } catch (err) {
        console.error('ERROR initInternetServices = ' + JSON.stringify(err));
      }
    }
  }

  showLocaleTip(): void {
    this.countLocaleTip++;
    if (this.countLocaleTip == 7)
      this.toast.showLongBottom(CONFIGS.localeTip).subscribe();
  }

  showAppRate() {
    this.appRate.preferences.storeAppURL = {
      ios: CONFIGS.app_rate_ios,
      android: CONFIGS.app_rate_android
    };
    this.appRate.promptForRating(true);
  }

  initTracking() {
    this.ga.startTrackerWithId(CONFIGS.google_analytics)
      .then(() => {
        // console.log('Google analytics is ready now');
        this.isGoogleAnalyticsReady = true;
        this.ga.trackView('Home_page');
      })
      .catch(e => console.error('Error starting GoogleAnalytics = ' + JSON.stringify(e)));
  }

  trackPage(page: string) {
    // must make sure that the Google Analytics is already init
    if (this.isGoogleAnalyticsReady) {
      // console.log('trackPage = ' + page);
      this.ga.trackView(page);
    }
  }

  getInverseLocale = (locale: string) => locale == LOCALES.US.code ? LOCALES.GB.code : LOCALES.US.code;
}

