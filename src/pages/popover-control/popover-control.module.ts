import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PopoverControlPage} from './popover-control';

@NgModule({
  declarations: [
    PopoverControlPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverControlPage),
  ],
  entryComponents: [
    PopoverControlPage
  ]
})
export class PopoverControlModule {}
