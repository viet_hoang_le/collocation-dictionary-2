import {Component, OnInit} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import {ServiceStorage} from '../../providers/atom/service-storage';
import {ServiceHelper} from '../../providers/service-helper';
import {CONFIGS, STORE} from '../../configs';

@IonicPage({
  name: 'popover-control'
})
@Component({
  templateUrl: './popover-control.html',
})
export class PopoverControlPage implements OnInit {

  speed: number = 1;
  fontsize: number = 62.5;
  background: string;
  isDualTranscript: boolean = false;

  constructor(public navParams: NavParams,
              public serviceStorage: ServiceStorage,
              public serviceHelper: ServiceHelper) {

    this.speed = this.serviceStorage.speed;
    this.fontsize = this.serviceStorage.fontsize;
    this.isDualTranscript = this.serviceStorage.isDualTranscript;
  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('Control_popover');
  }

  onclick_speedIncrease() {
    this.speed = (this.speed * 10 + 1) / 10;
    this.serviceStorage.setStorage(STORE.speed, this.speed);
    this.serviceHelper.tts_text(CONFIGS.default_speak_speed + this.speed);
  }

  onclick_speedDecrease() {
    this.speed = (this.speed * 10 - 1) / 10;
    this.serviceStorage.setStorage(STORE.speed, this.speed);
    this.serviceHelper.tts_text(CONFIGS.default_speak_speed + this.speed);
  }

  onclick_fontIncrease() {
    this.fontsize = (this.fontsize * 10 + 5) / 10;
    this.serviceStorage.setStorage(STORE.fontsize, this.fontsize);

    let html = document.getElementsByTagName('html');
    html.item(0).style.fontSize = this.fontsize + '%';
  }

  onclick_fontDecrease() {
    this.fontsize = (this.fontsize * 10 - 5) / 10;
    this.serviceStorage.setStorage(STORE.fontsize, this.fontsize);

    let html = document.getElementsByTagName('html');
    html.item(0).style.fontSize = this.fontsize + '%';
  }

  changeBackground(color) {
    this.background = color;
    this.serviceStorage.setStorage(STORE.color, color);
  }

  onclick_dualTranscript() {
    this.isDualTranscript = !this.isDualTranscript;
    this.serviceStorage.setStorage(STORE.isDual, this.isDualTranscript);
  }

}
