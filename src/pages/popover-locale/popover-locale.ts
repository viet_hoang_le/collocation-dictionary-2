import {Component} from '@angular/core';
import {IonicPage, ViewController} from 'ionic-angular';
import {ServiceStorage} from '../../providers/atom/service-storage';
import {DEFAULT, LOCALES, STORE} from '../../configs';

@IonicPage({
  name: 'popover-locale'
})
@Component({
  templateUrl: './popover-locale.html',
})
export class PopoverLocalePage {

  setting = this.serviceStorage.setting;

  public locales_default: string[];
  public jsonLocales;

  constructor(public viewCtrl: ViewController,
              public serviceStorage: ServiceStorage) {
    this.locales_default = DEFAULT.locales_full;
    this.jsonLocales = LOCALES;
  }

  toggleHidePopover() {
    this.serviceStorage.setStorage(STORE.isPopover, false);
    this.viewCtrl.dismiss();
  }

  onclick_selectLocale(locale) {
    this.serviceStorage.setStorage(STORE.locale, locale);
  }

}
