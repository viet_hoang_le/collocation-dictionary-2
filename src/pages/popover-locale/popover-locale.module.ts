import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PopoverLocalePage} from './popover-locale';

@NgModule({
  declarations: [
    PopoverLocalePage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverLocalePage),
  ],
  entryComponents: [
    PopoverLocalePage
  ]
})
export class PopoverLocaleModule {}
