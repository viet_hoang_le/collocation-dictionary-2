import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';

@IonicPage({
  name: 'bookmark-history'
})
@Component({
  templateUrl: 'bookmark-history.html',
})
export class BookmarkHistoryPage {

  tab_bookmark = 'tab-bookmark';
  tab_history = 'tab-history';

  constructor(public navCtrl: NavController) {
  }

}
