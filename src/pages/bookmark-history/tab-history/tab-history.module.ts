import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TabHistoryPage} from './tab-history';

@NgModule({
  declarations: [
    TabHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(TabHistoryPage),
  ],
  entryComponents: [
    TabHistoryPage
  ]
})
export class TabHistoryModule {}
