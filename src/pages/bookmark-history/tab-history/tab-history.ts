import {Component, ElementRef, ViewChild, OnInit, AfterViewInit} from '@angular/core';
import {NavController, Events, IonicPage} from 'ionic-angular';
import {ServiceHelper} from '../../../providers/service-helper';
import {ServiceDictionary} from '../../../providers/service-dictionary';
import {EVENT_STORAGE_UPDATED, ServiceStorage} from '../../../providers/atom/service-storage';
import {LOCALES, TABLE} from '../../../configs';
import {Word, WordList} from '../../../types/word';

@IonicPage({
  name: 'tab-history'
})
@Component({
  templateUrl: 'tab-history.html',
})
export class TabHistoryPage implements OnInit, AfterViewInit {

  setting = this.serviceStorage.setting;

  public items: Word[] = [];
  public itemList: WordList = {};
  public isIPA_display: boolean = true;

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(private navCtrl: NavController,
              public events: Events,
              public serviceHelper: ServiceHelper,
              public serviceDictionary: ServiceDictionary,
              public serviceStorage: ServiceStorage) {

    this.listenToEvents();
  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('History_Tab_page');

    this.isIPA_display = this.serviceStorage.isIPA_display;

    this.serviceStorage.loadBookmarkHistory(TABLE.HISTORY, 0).then(result => {
      this.items = result;
      for (const word of this.items) {
        this.itemList[word.wordId] = word;
      }
      this.serviceDictionary.loadPronunciations(this.itemList, this.setting.locale);

      this.serviceStorage.checkingBookmarks(Object.keys(this.itemList)).then(bookmarkedIds => {
        for (let id of bookmarkedIds) {
          this.itemList[id].isBookmarked = true;
        }
      });
    });
  }

  ngAfterViewInit(): void {
    this.serviceStorage.loadTheme(this.skinContent);
  }

  listenToEvents() {

    this.events.subscribe(EVENT_STORAGE_UPDATED, () => {

      this.serviceDictionary.loadPronunciations(this.itemList, this.setting.locale).then(isSucceed => {
        if (isSucceed) {
          this.items = [];
          for (let key of Object.keys(this.itemList)) {
            this.items.push(this.itemList[key]);
          }
        }
      });
      this.serviceStorage.loadTheme(this.skinContent);
    });
  }

  onclick_word = (wordItem: Word) => this.navCtrl.push('word-detail', {item: wordItem});

  onclick_popoverLocale = (ev) => this.serviceHelper.onclickPopoverLocale(ev);

  onclick_clearAll(ev) {
    this.items = [];
    this.itemList = {};
    this.serviceStorage.deleteAllBookmarkHistory(TABLE.HISTORY);
  }

  onclick_toggleBookmark(wordItem: Word) {

    wordItem.isBookmarked = !wordItem.isBookmarked;
    this.serviceStorage.setBookmarkHistory(TABLE.BOOKMARK, wordItem);

  }

}
