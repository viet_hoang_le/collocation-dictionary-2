import {Component, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {Toast} from '@ionic-native/toast';
import {ServiceDictionary} from '../../providers/service-dictionary';
import {EVENT_STORAGE_READY, EVENT_STORAGE_UPDATED, ServiceStorage, EVENT_DATABASE_READY} from '../../providers/atom/service-storage';
import {ServiceHelper} from '../../providers/service-helper';
import {
  Events, NavController, Content, ModalController, LoadingController,
  IonicPage
} from 'ionic-angular';
import {CONFIGS, COLORS, TABLE, LOCALES, STORE} from '../../configs';
import {Word, WordList} from '../../types/word';
import {Utils} from '../../utils';
import {Navbar} from '../../components/navbar/navbar.component';

@IonicPage({
  name: 'home'
})
@Component({
  templateUrl: 'homepage.html'
})
export class HomePage implements AfterViewInit {

  setting = this.serviceStorage.setting;

  items: Word[] = [];
  wordList: WordList = {};
  locale_inverse = LOCALES.GB.code;
  isIPA_display = true;
  isWordRandom = true;
  isDictReady = false;
  isTTS_visible = false;
  loadMoreStatus = [0, 0];  // 1st element is count of words, 2nd is isShow loading more or not
  tts_text: string;
  isDualTranscript = false;
  currentIndex = -1;
  sentences: string[] = [];

  SHOW_LOAD_MORE_YES = 1;
  SHOW_LOAD_MORE_NO = 0;

  @ViewChild(Content) content: Content;
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(private modalCtrl: ModalController,
              private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private toast: Toast,
              private serviceDictionary: ServiceDictionary,
              private serviceStorage: ServiceStorage,
              private serviceHelper: ServiceHelper,
              private events: Events) {

    this.locale_inverse = Utils.getInverseLocale(this.setting.locale);
    this.isIPA_display = this.serviceStorage.isIPA_display;
    this.isWordRandom = this.serviceStorage.isWordRandom;
    this.isDualTranscript = this.serviceStorage.isDualTranscript;
    this.isDictReady = this.serviceStorage.isDictReady;

    this.listenToEvents();

    // for testing
    /*for (let i = 1; i < 5; i++) {
      const word = new Word(i, 'test_' + i);
      word.type = '2';
      word.setIPA('US', 'testUS_'+i);
      word.setIPA('GB', 'testGB_'+i);
      this.items.push(word);
    }*/
  }

  ngAfterViewInit(): void {
    this.serviceHelper.trackPage('Home_page');

    this.loadWordList();

    this.serviceStorage.loadTheme(this.skinContent);
  }

  listenToEvents() {

    this.events.subscribe(EVENT_DATABASE_READY, () => {

      this.isDictReady = this.serviceStorage.isDictReady;
      this.loadWordList();

    });

    this.events.subscribe(EVENT_STORAGE_READY, () => {
      this.locale_inverse = Utils.getInverseLocale(this.setting.locale);
      this.isIPA_display = this.serviceStorage.isIPA_display;
      this.isWordRandom = this.serviceStorage.isWordRandom;
      this.isDualTranscript = this.serviceStorage.isDualTranscript;

      const fontsize = this.serviceStorage.fontsize;

      const html = document.getElementsByTagName('html');
      if (html.length > 0) {
        html.item(0).style.fontSize = fontsize + '%';
      }

      this.loadWordList();

      // set to current installed version if this is the first time the app is opened after updated
      if (!this.serviceStorage.isLatestVersion()) {
        const profileModal = this.modalCtrl.create('modalBegin');
        profileModal.present();
        this.serviceStorage.setStorage(STORE.version, CONFIGS.app_version);
      }
    });

    this.events.subscribe(EVENT_STORAGE_UPDATED, () => {
      this.locale_inverse = Utils.getInverseLocale(this.setting.locale);
      this.isIPA_display = this.serviceStorage.isIPA_display;
      this.isWordRandom = this.serviceStorage.isWordRandom;
      this.isDualTranscript = this.serviceStorage.isDualTranscript;

      this.serviceStorage.loadTheme(this.skinContent);

    });
  }

  loadWordList(searchText = '', isLoadingMore = false): Promise<boolean> {
    // console.log('isDictReady = ' + this.serviceStorage.isDictReady);
    // console.log('isSettingReady = ' + this.serviceStorage.isSettingReady);
    return new Promise(resolve => {
      if (this.serviceStorage.isDictReady && this.serviceStorage.isSettingReady) {
        let index = 0;
        let limit = CONFIGS.LIMIT_DEFAULT;
        const loadMoreStt = this.loadMoreStatus;

        if (isLoadingMore == true) {
          limit = CONFIGS.LIMIT_MORE;
          if (!this.isWordRandom || searchText.length > 0) {
            index = this.items.length;
          }
        }

          this.serviceDictionary.searchWords(searchText, index, limit, this.isWordRandom)
          .then(wordList => {
            this.updateWordList(wordList, isLoadingMore);
            /**
             * Compare the old number of items (before load) to current number of items
             * or If the loading-more number of items is smaller than what we expected
             */
            const itemsCount = Object.keys(wordList).length;
            loadMoreStt[1] = loadMoreStt[0] == this.items.length
            || itemsCount < limit ? this.SHOW_LOAD_MORE_NO : this.SHOW_LOAD_MORE_YES;
            loadMoreStt[0] = itemsCount;

            // loading pronunciations of wordList
            this.serviceDictionary.loadPronunciations(this.wordList, LOCALES.US.code)
              .then(isSucceed => {
              if (isSucceed)
                this.updateWordList(this.wordList);
            });
            this.serviceDictionary.loadPronunciations(this.wordList, LOCALES.GB.code)
              .then(isSucceed => {
              if (isSucceed)
                this.updateWordList(this.wordList);
            });
            resolve(true);
          });
      }
      else {
        resolve(false);
      }
    })
  }

  onSearch(searchText: string) {
    this.loadWordList(searchText);
    if (searchText.length == 0)
      this.sentences = [];
    else
      this.serviceDictionary.searchSentences(searchText, 0, 2)
        .then(sentenceArr => this.sentences = sentenceArr);
  }

  /**
   * Update the array of items from wordList and check bookmarked ones
   * @param wordList
   * @param isItemsAdd
   */
  updateWordList(wordList: WordList, isItemsAdd = false): void {
    if (!wordList || Object.keys(wordList).length == 0) {
      if (!isItemsAdd) {
        this.wordList = {};
        this.items = [];
      }
    }
    else {
      if (!isItemsAdd) {
        this.items = [];
        this.wordList = wordList;
      }
      else
        Object.assign(this.wordList, wordList);

      for (const key of Object.keys(wordList)) {
        this.items.push(wordList[key]);
      }
      if (this.isWordRandom) {
        this.items.sort((a, b) => {
          const aValue = a.word;
          const bValue = b.word;
          return (aValue.length - bValue.length);
        });
      }
      this.serviceStorage.checkingBookmarks(Object.keys(wordList))
        .then(function (bookmarkedIds) {
          for (const id of bookmarkedIds) {
            wordList[id].isBookmarked = true;
          }
        });
    }
  }

  onclick_word(index: number, wordItem, isNaturalTTS: boolean = false) {
    this.currentIndex = index;
    this.serviceHelper.onclickWord(wordItem, isNaturalTTS);
  }

  onclick_word_inverse(index: number, wordItem, isNaturalTTS: boolean = false) {
    this.currentIndex = index;
    this.serviceHelper.onclickWord(wordItem, isNaturalTTS, Utils.getInverseLocale(this.setting.locale));
  }

  onclick_word_detail(wordItem: Word) {
    this.serviceStorage.setBookmarkHistory(TABLE.HISTORY, wordItem);
    this.navCtrl.push('word-detail', {item: wordItem});
  }

  onclick_toggleBookmark(wordItem: Word) {
    wordItem.isBookmarked = !wordItem.isBookmarked;
    if (wordItem.isBookmarked) {
      this.serviceStorage.setBookmarkHistory(TABLE.BOOKMARK, wordItem);
    } else {
      this.serviceStorage.deleteBookmarkHistory(TABLE.BOOKMARK, wordItem.wordId);
    }
  }

  onclick_toggleTTS() {
    this.isTTS_visible = !this.isTTS_visible;
    if (this.isTTS_visible) {
      this.content.scrollToTop(300);
    }
  }

  onclick_tts_text() {
    if (this.tts_text) {
      this.serviceHelper.tts_text(this.tts_text);
    }
  }

  onclick_tts_human(locale) {
    if (this.tts_text) {
      this.serviceHelper.naturalTTS(this.tts_text, locale);
    }
  }

  onclick_resetup() {
    const loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present();
    this.serviceHelper.deployDBs().then(isDeployed => {
      if (isDeployed) {
        this.serviceDictionary.openDatabase().then(isDictReady => {
          if (isDictReady)
            this.serviceStorage.setDictReady();
        });
      } else {
        this.toast.showLongBottom('Failed to deploy the dictionaries. Unknown reason :(').subscribe();
      }
      loading.dismiss();
    });
  }

  doRefresh = (refresher) => this.loadWordList().then(() => refresher.complete());

  getStyleSelecting(index): string {
    const color = this.serviceStorage.color;
    return this.currentIndex == index ? COLORS[color].highlight : 'inherit';
  }

  gotoSentencePage = () => this.navCtrl.push('sentences', {item: this.navBar.searchText});

}
