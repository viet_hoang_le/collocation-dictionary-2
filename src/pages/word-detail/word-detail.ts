import {Component, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {NavParams, Events, IonicPage} from 'ionic-angular';
import {ServiceDictionary} from '../../providers/service-dictionary';
import {ServiceHelper} from '../../providers/service-helper';
import {EVENT_STORAGE_UPDATED, ServiceStorage} from '../../providers/atom/service-storage';
import {IWordRefs, Word, WordList} from '../../types/word';
import {LOCALES, TABLE} from '../../configs';

@IonicPage({
  name: 'word-detail'
})
@Component({
  templateUrl: 'word-detail.html',
})
export class WordDetailPage implements AfterViewInit {

  word: Word;
  isExpanded = true;

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(private events: Events,
              private params: NavParams,
              private serviceStorage: ServiceStorage,
              private serviceHelper: ServiceHelper,
              private serviceDictionary: ServiceDictionary) {

    this.word = this.params.get('item');

    this.loadWord();

    this.listenToEvents();

    this.serviceHelper.showAdmobInterstitial();
  }

  ngAfterViewInit(): void {
    this.serviceHelper.trackPage('Word_Detail_page');
    this.serviceStorage.loadTheme(this.skinContent);
  }

  listenToEvents() {
    this.events.subscribe(EVENT_STORAGE_UPDATED, () => {
      this.serviceStorage.loadTheme(this.skinContent);
    });
  }

  private loadWord() {
    const wordList: WordList = {};
    wordList[this.word.wordId] = this.word;

    this.serviceDictionary.loadPronunciations(wordList, LOCALES.US.code);
    this.serviceDictionary.loadPronunciations(wordList, LOCALES.GB.code);

    this.serviceDictionary.loadWordMeaning(this.word.wordId).then(wordMeans => {
      this.word.means = wordMeans;
      this.serviceDictionary.loadMeanUsage(this.word.wordId, this.word.means);
    });
    this.serviceDictionary.loadWordRef(this.word.wordId).then(refs => {
      this.word.refs = refs;
    });
  }

  onclick_word = (word, locale) => this.serviceHelper.tts_text(word, locale);
  onclick_sentence = sentence => this.serviceHelper.tts_text(sentence);
  onclick_word_natural = (word, locale) => this.serviceHelper.naturalTTS(word, locale);

  onclick_toggleBookmark() {
    this.word.isBookmarked = !this.word.isBookmarked;
    if (this.word.isBookmarked)
      this.serviceStorage.setBookmarkHistory(TABLE.BOOKMARK, this.word);
    else
      this.serviceStorage.deleteBookmarkHistory(TABLE.BOOKMARK, this.word.wordId);
  }

  goToRef(ref: IWordRefs) {
    if (parseInt(ref.pageType) != 10) {
      // only process the ref which is not a PDF type
      this.serviceDictionary.findWord(ref.page, ref.pageType).then(word => {
        this.word = word;
        this.loadWord();
      });
    }
  }
}
