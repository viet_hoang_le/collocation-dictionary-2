import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {WordDetailPage} from './word-detail';
import {PipeModule} from '../../providers/filter.pipe.module';
import {NavbarModule} from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    WordDetailPage,
  ],
  imports: [
    NavbarModule,
    PipeModule.forRoot(),
    IonicPageModule.forChild(WordDetailPage),
  ],
  entryComponents: [
    WordDetailPage
  ]
})
export class WordDetailModule {}
