import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SentencesPage} from './sentences';
import {NavbarModule} from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    SentencesPage,
  ],
  imports: [
    NavbarModule,
    IonicPageModule.forChild(SentencesPage),
  ],
  entryComponents: [
    SentencesPage
  ]
})
export class SentencesModule {
}
