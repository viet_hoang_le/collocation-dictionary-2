import {Component, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {Events, IonicPage, NavParams} from 'ionic-angular';
import {ServiceHelper} from '../../providers/service-helper';
import {ServiceDictionary} from '../../providers/service-dictionary';
import {EVENT_STORAGE_UPDATED, ServiceStorage} from '../../providers/atom/service-storage';
import {LOCALES, CONFIGS} from '../../configs';

@IonicPage({
  name: 'sentences'
})
@Component({
  templateUrl: 'sentences.html',
})
export class SentencesPage implements AfterViewInit {

  setting = this.serviceStorage.setting;

  searchText: string;
  items: string[] = [];

  loadMoreStatus = [0, 0];  // 1st element is count of words, 2nd is isShow loading more or not
  SHOW_LOAD_MORE_YES = 1;
  SHOW_LOAD_MORE_NO = 0;

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(private events: Events,
              private params: NavParams,
              private serviceHelper: ServiceHelper,
              private serviceDictionary: ServiceDictionary,
              private serviceStorage: ServiceStorage) {

    this.searchText = this.params.get('item');
    this.items = [];

    this.searchSentences(false);

    this.listenToEvents();
  }

  ngAfterViewInit(): void {
    this.serviceHelper.trackPage('Sentences_page');
    this.serviceStorage.loadTheme(this.skinContent);
  }

  listenToEvents() {
    this.events.subscribe(EVENT_STORAGE_UPDATED, () => {
      this.serviceStorage.loadTheme(this.skinContent);
    });
  }

  searchSentences(isLoadingMore): void {
    let index = 0;
    let limit = CONFIGS.LIMIT_DEFAULT;
    if (isLoadingMore == true) {
      limit = CONFIGS.LIMIT_MORE;
      index = this.items.length;
    }
    else {
      this.items = [];
      this.loadMoreStatus = [0, 0];
    }
    const loadMoreStt = this.loadMoreStatus;

    this.serviceDictionary.searchSentences(this.searchText, index, limit)
      .then(sentences => {
        this.items = this.items.concat(sentences);
        /**
         * Compare the old number of items (before load) to current number of items
         * or If the loading-more number of items is smaller than what we expected
         */
        const itemsCount = sentences.length;
        loadMoreStt[1] = loadMoreStt[0] == this.items.length
        || itemsCount < limit ? this.SHOW_LOAD_MORE_NO : this.SHOW_LOAD_MORE_YES;
        loadMoreStt[0] = itemsCount;
      });
  }

  onSearch(searchText: string) {
    this.searchText = searchText;
    this.searchSentences(false);
  }

  speak(sentence: string, isNaturalTTS = false) {
    if (isNaturalTTS)
      this.serviceHelper.naturalTTS(sentence);
    else
      this.serviceHelper.tts_text(sentence);
  }

}
