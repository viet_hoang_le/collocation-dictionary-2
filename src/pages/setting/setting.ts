import {Component, ViewChild, ElementRef, OnInit, AfterViewInit} from '@angular/core';
import {NavController, Events, IonicPage} from 'ionic-angular';
import {Toast} from '@ionic-native/toast';
import {ServiceStorage} from '../../providers/atom/service-storage';
import {ServiceHelper} from '../../providers/service-helper';
import {STORE, DEFAULT, LOCALES} from '../../configs';

@IonicPage({
  name: 'setting'
})
@Component({
  templateUrl: 'setting.html',
})
export class SettingPage implements OnInit, AfterViewInit {

  setting = this.serviceStorage.setting;

  public isIPA_display = true;
  public isPopoverShow = true;
  public isWordRandom = true;
  public locales_default: string[];
  public jsonLocales;

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(public navCtrl: NavController,
              public toast: Toast,
              public serviceStorage: ServiceStorage,
              public serviceHelper: ServiceHelper,
              public events: Events) {
    this.jsonLocales = LOCALES;
  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('Setting_page');

    this.loadConfigs();

  }

  ngAfterViewInit(): void {
    this.serviceStorage.loadTheme(this.skinContent);
  }

  public loadConfigs() {

    this.isIPA_display = this.serviceStorage.isIPA_display;
    this.locales_default = DEFAULT.locales_full;

    this.isPopoverShow = this.serviceStorage.isPopoverShow;
    this.isWordRandom = this.serviceStorage.isWordRandom;
  }

  toggleIPA_display = () => this.serviceStorage.setStorage(STORE.isIPA, this.isIPA_display);

  toggleHidePopover = () => this.serviceStorage.setStorage(STORE.isPopover, this.isPopoverShow);

  toggleWordRandom = () => this.serviceStorage.setStorage(STORE.isRandom, this.isWordRandom);

  toggleLocale(code) {
    let locales: string[] = this.setting.locales;
    if (locales.indexOf(code) > -1) {
      if (locales.length > 1) {
        locales.splice(locales.indexOf(code), 1);
      } else {
        this.toast.showShortBottom('You cannot deactivate all voices').subscribe();
        return;
      }
    } else {
      locales.push(code);
    }

    this.serviceStorage.setStorage(STORE.locales, locales);
  };
}
