import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SettingPage} from './setting';
import {NavbarModule} from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    SettingPage,
  ],
  imports: [
    NavbarModule,
    IonicPageModule.forChild(SettingPage),
  ],
  entryComponents: [
    SettingPage
  ]
})
export class SettingModule {}
