import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {ServiceDictionary} from '../../providers/service-dictionary';
import {ServiceStorage} from '../../providers/atom/service-storage';
import {ServiceHelper} from '../../providers/service-helper';
import {LOCALES, TABLE} from '../../configs';
import {Word, WordList} from '../../types/word';

@IonicPage({
  name: 'popover-history'
})
@Component({
  templateUrl: './popover-history.html',
})
export class PopoverHistoryPage implements OnInit {

  setting = this.serviceStorage.setting;
  public items: Word[] = [];
  public isIPA_display: boolean = true;

  constructor(private navCtrl: NavController,
              private serviceHelper: ServiceHelper,
              private serviceDictionary: ServiceDictionary,
              private serviceStorage: ServiceStorage) {
  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('History_popover');

    this.isIPA_display = this.serviceStorage.isIPA_display;
    this.serviceStorage.loadBookmarkHistory(TABLE.HISTORY, 9).then(result => {
      this.items = result;
      const itemList: WordList = {};
      for (const word of this.items) {
        itemList[word.wordId] = word;
      }
      this.serviceDictionary.loadPronunciations(itemList, this.setting.locale);
    });
  }

  onclick_history() {
    this.navCtrl.push('bookmark-history');
  }

  onclick_word = (wordItem: Word) => {
    this.navCtrl.remove(1, this.navCtrl.length());
    this.navCtrl.push('word-detail', {item: wordItem});
  };

}
