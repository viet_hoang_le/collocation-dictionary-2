import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PopoverHistoryPage} from './popover-history';

@NgModule({
  declarations: [
    PopoverHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverHistoryPage),
  ],
  entryComponents: [
    PopoverHistoryPage
  ]
})
export class PopoverHistoryModule {}
