import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app';

import {ServiceHelper} from '../providers/service-helper';
import {ServiceStorage} from '../providers/atom/service-storage';
import {ServiceDictionary} from '../providers/service-dictionary';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {GoogleAnalytics} from '@ionic-native/google-analytics';
import {File} from '@ionic-native/file';
import {NativeStorage} from '@ionic-native/native-storage';
import {SQLite} from '@ionic-native/sqlite';
import {Toast} from '@ionic-native/toast';
import {Network} from '@ionic-native/network';
import {TextToSpeech} from '@ionic-native/text-to-speech';
import {AdMob} from '@ionic-native/admob';
import {AppRate} from '@ionic-native/app-rate';
import {StreamingMedia} from '@ionic-native/streaming-media';
import {Zip} from '@ionic-native/zip';
import {Transfer} from '@ionic-native/transfer';

@NgModule({
  declarations: [
    MyApp,
  ],
  entryComponents: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      preloadModules: true
    }),
  ],
  exports: [
  ],
  bootstrap: [IonicApp],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleAnalytics,
    NativeStorage,
    File,
    SQLite,
    Toast,
    Network,
    TextToSpeech,
    AdMob,
    AppRate,
    StreamingMedia,
    Zip,
    Transfer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceHelper,
    ServiceStorage,
    ServiceDictionary,
  ]
})
export class AppModule {
}
