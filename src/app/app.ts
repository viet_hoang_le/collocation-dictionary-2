import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, Events, LoadingController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ServiceHelper} from '../providers/service-helper';
import {ServiceDictionary} from '../providers/service-dictionary';
import {EVENT_STORAGE_READY, ServiceStorage} from '../providers/atom/service-storage';
import {CONFIGS} from './../configs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'home';

  pages: Array<{ title: string, icon: string, name: string, color?: string }>;

  public appName: string;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private loadingCtrl: LoadingController,
              private events: Events,
              private serviceStorage: ServiceStorage,
              private serviceHelper: ServiceHelper,
              private serviceDictionary: ServiceDictionary
  ) {

    this.initializeApp();

    this.appName = CONFIGS.appName;

    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Home', icon: 'ios-home', name: 'home'},
      {title: 'History & Bookmark', icon: 'ios-timer-outline', name: 'bookmark-history', color: 'blacklight'},
      {title: 'Setting', icon: 'md-settings', name: 'setting', color: 'blacklight'},
      {title: 'How To', icon: 'md-bulb', name: 'howto', color: 'blacklight'},
      {title: 'About', icon: 'ios-information-circle-outline', name: 'about', color: 'blacklight'}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.serviceStorage.loadingConfiguration();
      this.listenToEvents();

      this.serviceHelper.initTracking();
    });
  }

  /**
   * Trying to deploy the Dictionaries and open them
   */
  private deployDBs() {
    const loading = this.loadingCtrl.create({
      content: 'Loading dictionaries...'
    });
    loading.present();
    this.serviceHelper.deployDBs().then(isDeployed => {
      if (isDeployed) {
        this.serviceDictionary.openDatabase().then(isDictReady => {
          if (isDictReady)
            this.serviceStorage.setDictReady();
        });
      }
      loading.dismiss();
    });
  }

  listenToEvents() {
    this.events.subscribe(EVENT_STORAGE_READY, () => {
      // need to wait for loading configs to figure out whether we need to deploy databases or not
      this.deployDBs();
    });
    setTimeout(() => {
      this.serviceHelper.initInternetServices();
    }, 3000);
  }

  openPage(page) {
    /**
     * Reset the content nav to have just this page
     * we wouldn't want the back button to show in this scenario
     */
    this.nav.setRoot(page.name);
  }
}
